# install nodejs
sudo apt-get -y install nodejs
# create soft link
sudo ln -s /usr/bin/nodejs /usr/bin/node
# install npm
curl http://npmjs.org/install.sh | sudo sh
# install webpack
npm install webpack -g
